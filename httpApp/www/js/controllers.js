angular.module('starter.controllers', [])

//Home Controller
.controller('DashController', function($scope, $http) {
    $scope.init = function () {
        $http.get('http://0.0.0.0:5050/').then(function(resp) {
            $scope.items = resp.data;
            window.localStorage.setItem("items", JSON.stringify(resp.data));
        }, function(err) {
            console.error('ERR', err);
        });
    };
    
    $scope.change = function(id, items) { 
        window.localStorage['items'] = JSON.stringify(items);
    };
    
    $scope.init();
})

//Data Controller
.controller('DataController', function($scope, $http) {
    $scope.init = function () {
        $scope.items = JSON.parse(window.localStorage['items'] || '{}');
    };
    
    $scope.sync = function (items) {
        $http.get('http://0.0.0.0:5050/sync/' + items).then(function(resp) {
            $scope.items = resp.data;
        }, function(err) {
            alert('error');
        });
    };
    
    $scope.$on('$stateChangeSuccess', function(){
        $scope.init();
    });
})